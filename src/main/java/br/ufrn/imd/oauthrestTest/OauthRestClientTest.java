package br.ufrn.imd.oauthrestTest;

import static org.junit.Assert.*;

import org.junit.Test;

import br.ufrn.imd.oauthrest.OauthRestHelper;

public class OauthRestClientTest {

	/**
	 * Url de acesso ao serviço de autorização.
	 */
	private String URL = "http://localhost:8080/AuthServer/auth";

	/**
	 * Caso de teste para requisição de um token com credenciais válidas.
	 */
	@Test
	public void testRequestWithValidCredentials() {
		String clientId = "TESTE_ID";
		String clientSecret = "testesecret";
		OauthRestHelper oauthHelper = OauthRestHelper.getInstance();
		String token = oauthHelper.requestTokenApp(URL, clientId, clientSecret);
		assertNotNull(token);
	}

	/**
	 * Caso de teste para requisição de um token com credenciais não válidas.
	 */
	@Test
	public void testRequestWithNotValidCredentials() {
		String clientId = "TESTE";
		String clientSecret = "testesecret";
		OauthRestHelper oauthHelper = OauthRestHelper.getInstance();
		String token = oauthHelper.requestTokenApp(URL, clientId, clientSecret);
		assertNull(token);
	}

}
